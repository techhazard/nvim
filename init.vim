"let $VIMRUNTIME = '~/git/neovim/runtime'
"let g:airline#extensions#tabline#enabled = 1
let mapleader = "\<Space>"
let normalcolorscheme = "iceberg"
let diffcolorscheme = "iceberg"

syntax enable
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/plugged')
" some sensible defaults
Plug 'tpope/vim-sensible'
"status bar at the bottom
Plug 'bling/vim-airline'

"colorschemes
Plug 'vim-scripts/xoria256.vim'
Plug 'nanotech/jellybeans.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'flazz/vim-colorschemes' " a whole bunch
Plug 'NLKNguyen/papercolor-theme'
Plug 'wellsjo/wells-colorscheme.vim'
Plug 'romainl/Apprentice'

Plug 'tpope/vim-fugitive' " git commands (:Gdiff !)
Plug 'airblade/vim-gitgutter' " git status in statusline
Plug 'jeetsukumaran/vim-buffersaurus' " regex search through all buffers with :Bsgrep
Plug 'hoelzro/vim-split-navigate' " move through visible lines with bimary search-like movement
let g:splitnavigate_start_key = "<leader>s"
let g:splitnavigate_up_key = "u"
let g:splitnavigate_down_key = "d"
let g:splitnavigate_abort_key = "q"

" open file at a line from shell:
" `$ vim path/to/file.txt:20` will open the file at line 20
Plug 'bogado/file-line'

Plug 'christoomey/vim-tmux-navigator' " navigate seamlessly between tmux & vim. requires tmux config (see repo)

Plug 'kburdett/vim-nuuid' " generate UUIDs
Plug 'majutsushi/tagbar' " display tags in a window
Plug 'pangloss/vim-javascript', {'for': 'javascript'} " better javascript highlighting
Plug 'mxw/vim-jsx' " highlighting for JSX (react)
Plug 'roryokane/detectindent' " detect indentation of the current file
Plug 'rust-lang/rust.vim', {'for': 'rust'} " rust highlighting
Plug 'scrooloose/nerdcommenter' " (un)comment code easily
Plug 'tmhedberg/SimpylFold', {'for': 'python' } " better folding for python
Plug 'manasthakur/VimSessionist' " better session management: SS->save SO->open SP->restore prev SL->list SD->delete
Plug 'thirtythreeforty/lessspace.vim' " strip trailing whitespace on edited lines only
let g:lessspace_blacklist = ['md', 'markdown']
Plug 'vim-scripts/IndentConsistencyCop' " detect mixed indents within a file
Plug 'mbbill/undotree' " undo tree visualiser with live diff
Plug 'cloudhead/neovim-fuzzy' " file finder using fzy. see 'cloudhead/neovim-fuzzy' and jhawthorn/fzy
Plug 'mmahnic/vim-flipwords' " flips words around a delimiter

"Plug 'mhinz/vim-startify'
"Plug 'octol/vim-cpp-enhanced-highlight'
"Plug 'scrooloose/nerdtree' " tree view for files
"Plug 'tpope/vim-five.git'
"Plug 'cazador481/fakeclip.neovim'
"Plug 'kien/ctrlp.vim'
"Plug 'vim-scripts/indentpython.vim'
"Plug 'vim-scriptsignore.git'
"Plug 'luochen1990/indent-detector.vim'

" markdown highlighting
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown', {'do': 'make' }

Plug 'kana/vim-arpeggio' " vim commands with simultaneous keypreses

if has('nvim') && has('python3')
  function! DoRemote(arg)
    UpdateRemotePlugins
  endfunction
  Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }
  let g:deoplete#enable_at_startup = 1
endif
if has('nvim')
  Plug 'neomake/neomake'
  autocmd InsertChange,TextChanged * update | Neomake
  let g:neomake_javascript_enabled_makers = ['./node_modules/.bin/eslint']
endif

if has('mac')
  " preview markdown on OSX
  Plug 'junegunn/vim-xmark', { 'do': 'make' }
endif

call plug#end()
let g:nuuid_no_mappings = 1
let g:undotree_WindowLayout = 4
let g:undotree_DiffAutoOpen = 1
let g:undotree_ShortIndicators = 1
let g:undotree_DiffpanelHeight = 10
let g:fuzzy_tabopen = 1
let g:fuzzy_jump_if_open = 1
let g:gitgutter_sign_column_always = 1
set t_Co=256
set timeoutlen=200

" always show sign column
autocmd BufEnter * sign define dummy
autocmd BufEnter * execute 'sign place 9999 line=1 name=dummy buffer=' . bufnr('')


colorscheme apprentice " for base colours of bottom bar

if &diff | exe 'colorscheme'.diffcolorscheme | else | exe 'colorscheme '. normalcolorscheme | endif
au FilterWritePre * if &diff | exe 'colorscheme '.diffcolorscheme | else | exe 'colorscheme '. normalcolorscheme | endif

set cursorline
set statusline=%{fugitive#statusline()}
set laststatus=2
set nu
set expandtab
set shiftwidth=2
set softtabstop=2
if has('persistent_undo')
  set undofile
  set undodir=$HOME/.nvim/undo
  set undolevels=1000
  set undoreload=10000
endif

let hlstate=0
nnoremap <silent> <leader>h :set hlsearch!<cr>
nnoremap <silent> <leader>u :UndotreeToggle\|UndotreeFocus<cr>
nnoremap <silent> <leader>o :FuzzyOpen<CR>
nnoremap <leader>f :Bsgrep<space>
let g:PaperColor_Light_Override = { 'background' : '#ffffff', 'cursorline' : '#dfdfff', 'matchparen' : '#d6d6d6' , 'comment' : '#8e908c' , 'linenumbers_fg': '#FF0000'}
nnoremap <silent> <BS> :TmuxNavigateLeft<cr>
nnoremap <silent> <BS> :TmuxNavigateLeft<cr>

call arpeggio#load()
Arpeggio inoremap jk  <Esc>

source ~/.config/nvim/tabline.vim
let g:PaperColor_Light_Override = { 'background' : '#ffffff', 'cursorline' : '#dfdfdf', 'matchparen' : '#d6d6d6' , 'comment' : '#8e908c' , 'linenumbers_fg': '#FF0000', 'diffadd_fg': '#00FF00', 'diffadd_bg': '#000000'}
set background=light
colorscheme PaperColor

" custom blue on white
highlight TopHighlight term=bold ctermfg=18 ctermbg=252
" custom red on white
highlight BottomHighlight term=bold ctermfg=88 ctermbg=252
